import fetch from '../config/fetch'
import axios from "axios";

export const createProject = async (data, id) => {
    return await fetch(process.env.VUE_APP_API_URL + '/projects/' + id, {
        method: 'POST',
        headers: {
            'Content-type': 'application/json',
            Accept: 'application/json',
        },
        body: JSON.stringify(data)
    }).then(res => {
        if (res != null) {
            return res
        }
    }).catch(err => {
        if (err.json) {
            return err.json.then(json => {
                console.log(json)
            })
        }
    })
}

export const addMemberToProject = async (data, id) => {
    return await fetch(process.env.VUE_APP_API_URL + '/projects/' + id, {
        method: 'POST',
        headers: {
            'Content-type': 'application/json',
            Accept: 'application/json',
        },
        body: JSON.stringify(data)
    }).then(res => {
        if (res != null) {
            return res
        }
    }).catch(err => {
        if (err.json) {
            return err.json.then(json => {
                console.log(json)
            })
        }
    })
}

export const deleteProject = async (data) => {
    JSON.parse(JSON.stringify(data))
}


export const getProjectAndUser = async (id) => {
    return await axios.get(process.env.VUE_APP_API_URL + '/projects/users/' + id).then((res) => {
        return res.data
    }).catch(err => {
        if (err.json) {
            return err.json.then(json => {
                console.log(json)
            })
        }
    })
}