import axios from "axios";

export const getNotifications = async (email) => {

    return await axios.get(process.env.VUE_APP_API_URL + '/notifications/' + email).then((res) => {
        return res.data
    }).catch(err => {
        if (err.json) {
            return err.json.then(json => {
                console.log(json)
            })
        }
    })
}
