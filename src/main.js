import Vue from 'vue'
import App from './App.vue'
import router from './config/router'
import Vmodal from "vue-js-modal";
import axios from 'axios';
import VueChatScroll from 'vue-chat-scroll'
import VueFormulate from '@braid/vue-formulate'
import './assets/css/formulate.css'


axios.defaults.headers.common['Content-type'] = 'application/json'
axios.defaults.headers.common['Accept'] = 'application/json'

Vue.config.productionTip = false
Vue.use(VueChatScroll)
Vue.use(Vmodal, { componentName: 'Modal' })
Vue.prototype.$axios = axios;
Vue.use(VueFormulate)

new Vue({
  router,
  render: h => h(App),
}).$mount('#app')
